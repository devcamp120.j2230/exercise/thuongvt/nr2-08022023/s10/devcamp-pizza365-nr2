//Khai báo thư viện express
const express = require("express");

//Khai báo middleware 
const {
    getAllOrderMiddleware,
    getAOrderMiddleware,
    postOrderMiddleware,
    putOrderMiddleware,
    deleteOrerMiddleware
    } = require(`../middleware/OrderMiddleware`);
// khai báo controller oder
const {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
} = require ("../controller/OrderController")
//Tạo router
const OrderRouter = express.Router();

//Sử dụng Router
OrderRouter.get("/Orders",getAllOrderMiddleware,getAllOrder);
OrderRouter.get("/Orders/:OrderId",getAOrderMiddleware,getOrderById);
OrderRouter.post("/Users/:UserId/Orders",postOrderMiddleware,createOrder);
OrderRouter.put("/Orders/:OrderId",putOrderMiddleware,updateOrderById);
OrderRouter.delete("/Orders/:OrderId",deleteOrerMiddleware,deleteOrderById);

module.exports = {OrderRouter}
