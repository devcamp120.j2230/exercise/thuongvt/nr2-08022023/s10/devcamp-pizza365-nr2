//Khai báo thư viện express
const express = require("express");

// Khai báo Middleware 
const {
    getAVoucherMiddleware,
    getAllVoucherMiddleware,
    postVoucherMiddleware,
    putVoucherMiddleware,
    deleteVoucherMiddleware
} =  require (`../middleware/VoucherMiddleware`);

// Khai báo controller
const {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
} = require("../controller/VoucherController")
// Tạo router
const VoucherRouter = express.Router();

// Sử dụng router
VoucherRouter.get("/Voucher",getAllVoucherMiddleware,getAllVoucher);

VoucherRouter.get("/Voucher/:VoucherId",getAVoucherMiddleware,getVoucherById);

VoucherRouter.post("/Voucher",postVoucherMiddleware,createVoucher);

VoucherRouter.put("/Voucher/:VoucherId",putVoucherMiddleware,updateVoucherById);

VoucherRouter.delete("/Voucher/:VoucherId",deleteVoucherMiddleware,deleteVoucherById)

module.exports = {VoucherRouter}
